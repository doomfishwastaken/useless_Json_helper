import json

dItems = {}

print("Input HELP for commands.")

while (True):
    strInput = input("> ")
    lInputCommands = strInput.split(";")

    for i in range(len(lInputCommands)):
        lInput = strInput.split(" ")
        lInput.append('')

        if (lInput[0] == "ADD"):
            try:
                if (len(lInput[1]) > 0):
                    try:
                        item = dItems[lInput[1]]
                        
                        if (len(lInput[2]) > 0):
                            try:
                                iAmt = int(lInput[2])

                                if (iAmt != 0):
                                    dItems[lInput[1]] = {"amount": item["amount"] + iAmt}
                                    print(f"Increased key {lInput[1]}'s amount with {iAmt}.")
                                else:
                                    print(f"Amount is 0 so nothing was added to key.")
                            except Exception:
                                print(f"Amount is not a real number.")
                        else:
                            dItems[lInput[1]] = {"amount": item["amount"] + 1}
                            print(f"Increased key {lInput[1]}'s amount with {1}.")
                    except Exception:
                        if (len(lInput[2]) > 0):
                            try:
                                iAmt = int(lInput[2])

                                if (iAmt != 0):
                                    dItems[lInput[1]] = {"amount": iAmt}
                                    print(f"Added new key called {lInput[1]} with amount {iAmt}.")
                                else:
                                    print(f"Amount is 0 so nothing was added nor was a new key created.")
                            except Exception:
                                print(f"Amount is not a number.")
                        else:
                            dItems[lInput[1]] = {"amount": 1}
                            print(f"Added new key called {lInput[1]} with amount {1}.")
            except Exception:
                print(f"Usage:\n|-ADD [KEY NAME] [AMOUNT]\nAdd to key's value, if no key exists create new one.\n|-If amount is not specified defaults to 1.")
        elif (lInput[0] == "REM"):
            try:
                if (len(lInput[1]) > 0):
                    try:
                        item = dItems[lInput[1]]
                        
                        if (len(lInput[2]) > 0):
                            try:
                                iAmt = int(lInput[2])

                                if (iAmt != 0):
                                    dItems[lInput[1]] = {"amount": item["amount"] - iAmt}
                                    print(f"Decreased key {lInput[1]}'s amount with {iAmt}.")
                                else:
                                    print(f"Amount is 0 so nothing was removed from key.")
                            except Exception:
                                print(f"Amount is not a number.")
                        else:
                            dItems[lInput[1]] = {"amount": item["amount"] - 1}
                            print(f"Decreased key {lInput[1]}'s amount with {1}.")
                    except Exception:
                        print(f"Key {lInput[1]} does not exist.")
            except Exception:
                print(f"Usage:\n|-REM [KEY NAME] [AMOUNT]\nDecrease key's value by amount.\n|-If amount is not specified defaults to 1.")
        elif (lInput[0] == "DEL"):
            try:
                if (len(lInput[1]) > 0):
                    try:
                        del dItems[lInput[1]]
                    except Exception:
                        print(f"Key {lInput[1]} does not exist.")
            except Exception:
                print(f"Usage:\nREM [KEY NAME]\n|-Deletes a key.")
        elif (lInput[0] == "SAVE"):
            del lInput[-1]
            try:
                if (len(lInput[1]) > 0):
                    with open(f"{lInput[1]}.json", "w") as f:
                        f.write(json.dumps(dItems, indent = 4))
                        print(f"Saved all keys to file {lInput[1]}.json.")
                        f.close
            except Exception as e:
                print(e)
                print(f"Usage:\nSAVE [FILE NAME]\n|-Saves all keys to file.")
        elif (lInput[0] == "PRINT"):
            print(json.dumps(dItems))
        elif (lInput[0] == "HELP"):
            print(f"Usage:\nADD [KEY NAME] [AMOUNT]\n|-Add to key's value, if no key exists create new one.\n|-If amount is not specified defaults to 1.")
            print(f"REM [KEY NAME] [AMOUNT]\n|-Decrease key's value by amount.\n|-If amount is not specified defaults to 1.")
            print(f"DEL [KEY NAME]\n|-Deletes a key.")
            print(f"SAVE [FILE NAME]\n|-Saves all keys to file.")
            print(f"PRINT\n|-Prints all keys.")
            print(f"HELP\n|-Shows this message.")
            print(f"QUIT\n|-Exits the program.")
        elif (lInput[0] == "QUIT"):
            exit()